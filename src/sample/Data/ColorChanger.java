package sample.Data;

import javafx.application.Platform;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import sample.Model.RectangleShape;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by AlZihad on 1/21/2017.
 */
public class ColorChanger
{
    RectangleShape rect1, rect2;
    int time1,time2;
    Paint paint1, paint2;
    Timer timer;
    WebView algorithm;
    public  ColorChanger(RectangleShape rectangleShape, RectangleShape rectangleShape2, WebView algo)
    {
        this.algorithm = algo;
        this.rect1 = rectangleShape;
        this.rect2 = rectangleShape2;
       // timer = new Timer();
       // timer.schedule(new InterchangeColor(), time*1000);
    }
    public  ColorChanger(RectangleShape rectangleShape)
    {
        this.rect1 = rectangleShape;
        // timer = new Timer();
        // timer.schedule(new InterchangeColor(), time*1000);
    }

    public void DoChangeColor(int time)
    {
        timer = new Timer();
        timer.schedule(new InterchangeColor(), time*1000);
    }



    public void UndoChangeColor(Paint paint1, Paint paint2, int time)
    {
        this.paint1 = paint1;
        this.paint2 = paint2;
        timer = new Timer();
        timer.schedule(new BackToPreviousColor(), time*1000);
    }

    class InterchangeColor extends TimerTask
    {
        public void run()
        {
            coloredAlgo(3);
            rect1.rectangle.setFill(Color.RED);
            rect2.rectangle.setFill(Color.RED);
        }
    }


    class BackToPreviousColor extends TimerTask
    {

        public void run()
        {
            coloredAlgo(2);
            rect1.rectangle.setFill(paint1);
            rect2.rectangle.setFill(paint2);
        }
    }

    // for pivot color
    public void DoChangeColorpiv(int time)
    {
        timer = new Timer();
        timer.schedule(new InterchangeColorpiv(), time*1000);
    }

    class InterchangeColorpiv extends TimerTask
    {
        public void run()
        {
            rect1.rectangle.setFill(Color.BLACK);
            coloredAlgo(3);
        }
    }

    public void UndoChangeColor_Piv(Paint paint1, int time)
    {
        this.paint1 = paint1;
        timer = new Timer();
        timer.schedule(new BackToPreviousColor_Piv(), time*1000);

    }

    class BackToPreviousColor_Piv extends TimerTask
    {

        public void run()
        {
            coloredAlgo(6);
            rect1.rectangle.setFill(paint1);
            //coloredAlgo(6);
        }
    }


    public void coloredAlgo(int line)
    {
        Platform.runLater(() -> {
            // Update UI here.
            String color[]={"#FFFFB0","#FFFFB0","#FFFFB0","#FFFFB0","#FFFFB0"};
            for (int i=1;i<=5;i++)
            {
                if (line ==i)
                {
                    color[i-1]="#FFFF00";//System.out.println(color[i-1]);
                }
                else
                {
                    color[i-1]="#FFFFA0";//System.out.println(color[i-1]);
                }
            }
            System.out.println("UI UPDATED from check");
            WebEngine webEngine = algorithm.getEngine();
            webEngine.loadContent("<html>\n" +
                    "<head>\n" +
                    "\t<title></title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<p id=\"code1\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;\"><span style=\"font-size:14px;\"><span data-mce-style=\"background-color: #ffffff;\"><span style=\"background-color:"+color[0]+";\">do</span></span></span></p>\n" +
                    "\n" +
                    "<p id=\"code2\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;\"><span style=\"font-size:14px;\"><span data-mce-style=\"background-color: #ffffff;\"><span style=\"background-color:"+color[1]+";\">&nbsp;for i = 1 to LastUnsortedElement</span></p>\n" +
                    "\n" +
                    "<p id=\"code4\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;\"><span style=\"font-size:14px;\"><span data-mce-style=\"background-color: #ffffff;\"><span style=\"background-color:"+color[2]+";\">&nbsp;&nbsp;&nbsp;&nbsp;if leftElement &gt; rightElement</span></span></span></p>\n" +
                    "\n" +
                    "<p id=\"code5\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;\"><span style=\"font-size:14px;\"><span data-mce-style=\"background-color: #ffffff;\"><span style=\"background-color:"+color[3]+";\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;swap(leftElement, rightElement)</span></span></span></p>\n" +
                    "\n" +
                    "<p id=\"code7\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;\"><span style=\"font-size:14px;\"><span data-mce-style=\"background-color: #ffffff;\"><span style=\"background-color:"+color[4]+";\">while swapped</span></span></span></p>\n" +
                    "</body>\n" +
                    "</html>\n");
        });


    }
}

// commit