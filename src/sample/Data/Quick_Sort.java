package sample.Data;

import javafx.scene.Group;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;
import sample.Model.RectangleShape;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by abida on 28/01/17.
 */
public class Quick_Sort {
    AnchorPane canvas;
    Color color[];
    Group root = new Group();
    Circle circle[];
    Rectangle rectangle[];
    Text text[];
    List<Integer> numbers;
    Rect_info swap1, swap2;
    Rect_info rect_info[];
    RectDesign rectDesign;
    int time = 0;
    WebView algorithm;

    public Quick_Sort(RectDesign rectDesign, WebView algo) {
        this.algorithm = algo;
        rect_info = rectDesign.rect_info;
        numbers = rectDesign.numbers;
        rectDesign.drawCanvasinWhite();
        rectDesign.inItColorArray();
        rectDesign.drawShapes();


        //startSorting();
    }

    public void startQuickSort()
    {
        System.out.println("Time's up!");
        int low = 0;
        int high = numbers.size() - 1;
        run(low,high);

    }




    public void run( int low, int high) {


        if (low >= high)
            return;

        //time =0;
        // pick the pivot
        int middle = low + (high - low) / 2;
        int pivot = numbers.get(middle);

        System.out.println(middle+"  ******** "+pivot);

        Rect_info rect_pivot = rect_info[middle];

        Paint piv_color = rect_pivot.color;
        //rect_pivot.rect.setFill(Color.BLACK);
        RectangleShape rectangleShape_piv = new RectangleShape(root, rect_pivot.rect,rect_pivot.text, algorithm);
        ColorChanger colorChanger_piv = new ColorChanger(rectangleShape_piv);


        // make left < pivot and right > pivot
        int i = low, j = high;
        while (i <= j) {
            colorChanger_piv.DoChangeColorpiv(time++);


            while (numbers.get(i) < pivot) {
                i++;
            }

            while (numbers.get(j) > pivot) {
                j--;
            }
            if (i <= j) {
                swap2 = rect_info[j];
                swap1 = rect_info[i];

                RectangleShape rectangleShape = new RectangleShape(root, swap1.rect, swap1.text,algorithm);
                RectangleShape rectangleShape1 = new RectangleShape(root, swap2.rect, swap2.text,algorithm);

                Paint paint1 = swap1.color;
                Paint paint2 = swap2.color;
                                   //  Exchange numbers

                ColorChanger colorChanger = new ColorChanger(rectangleShape,rectangleShape1,algorithm);

                colorChanger.DoChangeColor(time++);
                Rect_info temp2 = rect_info[j ];
                rect_info[j] = rect_info[i];
                rect_info[i] = temp2;

                int temp = numbers.get(i);
                numbers.set(i , numbers.get(j));
                numbers.set(j, temp);
                i++;
                j--;
                rectangleShape.interChangePosition(rectangleShape1,time++);
                colorChanger.UndoChangeColor(paint1,paint2,time++);
            }
            else
            {
               addIntentionalDelay(++time);
            }


        }


        if (low < j) {
            colorChanger_piv.UndoChangeColor_Piv(piv_color,time++);
           // System.out.println("recursively sort two sub parts11!");
            run(low, j);
        }

        if (high > i) {
            colorChanger_piv.UndoChangeColor_Piv(piv_color,time++);

            //System.out.println("recursively sort two sub parts22");
            run(i, high);

        }

    }
    class DelayTask extends TimerTask
    {
        public void run()
        {
            System.out.println("Delay ended");
        }
    }
    public void addIntentionalDelay(int time)
    {
        Timer timer, delayTimer;
        delayTimer = new Timer();
        delayTimer.schedule(new DelayTask(), time*10000);
    }


}
