package sample.Data;
import com.sun.javafx.tk.Toolkit;
import javafx.concurrent.Task;
import javafx.scene.Group;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import sample.Model.RectangleShape;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.TimerTask;



public class Bubble_Sort
{

    Group root = new Group();
    List<Integer> numbers;
    Rect_info swap1, swap2;
    Rect_info rect_info[];
    WebView algorithm;

    public Bubble_Sort(RectDesign rectDesign, WebView algorithm)
    {
        this.algorithm = algorithm;
        rect_info = rectDesign.rect_info;
        numbers = rectDesign.numbers;

        rectDesign.drawCanvasinWhite();
        rectDesign.inItColorArray();
        rectDesign.drawShapes();

    }

    public void startBubbleSort()
    {
        System.out.println("Time's up!");
        run();
    }

    public void run()
    {
        int time = 0;
        for (int i = 0; i < numbers.size(); i++)
        {
            for (int j = 0; j < numbers.size() - 1 - i; j++)
            {
                swap2 = rect_info[j + 1];
                swap1 = rect_info[j];

                RectangleShape rectangleShape = new RectangleShape(root, swap1.rect, swap1.text,algorithm);
                RectangleShape rectangleShape1 = new RectangleShape(root, swap2.rect, swap2.text,algorithm);

                Paint paint1 = swap1.color;
                Paint paint2 = swap2.color;

                ColorChanger colorChanger = new ColorChanger(rectangleShape,rectangleShape1, algorithm);
                colorChanger.DoChangeColor(time++);

                if (numbers.get(j) > numbers.get(j + 1))
                {
                    Rect_info temp = rect_info[j + 1];
                    rect_info[j + 1] = rect_info[j];
                    rect_info[j] = temp;
                    int temp2 = numbers.get(j + 1);
                    numbers.set(j + 1, numbers.get(j));
                    numbers.set(j, temp2);
                    swap1 = rect_info[j + 1];
                    swap2 = rect_info[j];
                    rectangleShape.interChangePosition(rectangleShape1,time++);
                }
                else
                {
                    rectangleShape.addIntentionalDelay(time++);
                }
               colorChanger.UndoChangeColor(paint1,paint2,time++);
            }
        }
    }
}
