package sample.Data;


import javafx.scene.Group;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.util.List;

class Rect_info
{
    Rectangle rect ;
    Text text;
    Color color;
}

public class RectDesign {
    AnchorPane canvas;
    Color color[];
    Group root = new Group();
    Circle circle[];
    Rectangle rectangle[];
    Text text[];
    List<Integer> numbers;
    Rect_info swap1, swap2;
    Rect_info rect_info[];
    
    public RectDesign(AnchorPane sort, List<Integer> numbers)
    {
        this.canvas = sort;
        this.numbers = numbers;
        rect_info = new Rect_info[10];
        //drawCanvasinWhite();
        //inItColorArray();
        //drawShapes();
    }
    void drawCanvasinWhite() {
        Rectangle Canvasrectangle = new Rectangle(320, 30, 655, 370);
        Canvasrectangle.setStroke(Color.BLACK);
        Canvasrectangle.setStrokeWidth(2.0);
        Canvasrectangle.setFill(Color.WHITE);
        root.getChildren().add(Canvasrectangle);
    }

    void inItColorArray()
    {
        color = new Color[10];
        color[0] = Color.BLUE;
        color[1] = Color.DARKGREY;
        color[2] = Color.GREEN;
        color[3] = Color.YELLOW;
        color[4] = Color.ORANGE;
        color[5] = Color.PINK;
        color[6] = Color.CHOCOLATE;
        color[7] = Color.BROWN;
        color[8] = Color.GOLD;
        color[9] = Color.CYAN;
    }



    void drawShapes() {
        rectangle = new Rectangle[100];
        text = new Text[10];
        for (int i = 0; i < numbers.size(); i++) {
            rectangle[i] = new Rectangle(362 + i * 60, calculateVerticalPosition(numbers.get(i) * 3), 35, numbers.get(i) * 3);
            rectangle[i].setStroke(Color.BLACK);
            rectangle[i].setStrokeWidth(2.0);
            rectangle[i].setFill(color[i]);

            text[i] = new Text(362 + i * 60 + 15, 395, "" + numbers.get(i));
            text[i].setFill(Color.GREEN);
            rect_info[i] = new Rect_info();
            rect_info[i].rect = rectangle[i];
            rect_info[i].rect = rectangle[i];
            rect_info[i].text = text[i];
            rect_info[i].color = color[i];

            root.getChildren().add(rectangle[i]);
            root.getChildren().add(text[i]);

        }
        canvas.getChildren().add(root);
    }

    int calculateVerticalPosition(int pos)
    {
        return 380 - pos;
    }

}
