package sample.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abida on 19/01/17.
 */
public class Array_Checker {
    private  int arraySize;
    List<Integer> intList;
    boolean errorDetected =false;
    String errorType;

    public Array_Checker(String input)
    {
        String array[]= input.split(",");
        this.arraySize = array.length;
        intList = new ArrayList<Integer>();

        for (int i=0;i<array.length;i++) {
            try {
                int x = Integer.parseInt(array[i].replace(" ", ""));
                if (x >= 100 || x < 0) {
                    errorDetected = true;
                    errorType = "Provide values between 0 and 99";
                } else if (!intList.contains(x)) {
                    intList.add(x);
                } else {
                    errorDetected = true;
                    errorType = "Value " + x + " appeared more than once!";
                }
            } catch (Exception e) {
                errorDetected = true;
                errorType = "Conversion Error!";
            }
        }
    }
    public String getErrorType()
    {
        return errorType;
    }
    public List<Integer> getIntList()
    {
        return intList;
    }

    public boolean getErrorDetected()
    {
        return errorDetected;
    }

}
