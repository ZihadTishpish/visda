package sample.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by AlZihad on 1/15/2017.
 */
public class RandomSeedArray
{
    private  int arraySize;
    List<Integer> intList;
    boolean errorDetected =false;
    String errorType;
    Array_Checker array_checker;

    public RandomSeedArray(int arraySize)
    {
        this.arraySize = arraySize;
    }

    public String getSeedArrayString()
    {
        intList = new ArrayList<Integer>();
        String generated="";
        for (int i=0;i<arraySize;)
        {
            int randomValue = getRandom();
            if (!intList.contains(randomValue))
            {
                intList.add(randomValue);
                if (i!=0)
                    generated+=", ";
                generated = generated+ randomValue;
                i++;
            }
        }
        return generated;
    }
    private int getRandom()
    {
        int random= new Random().nextInt()%100;
        return Math.abs(random);
    }


}
