package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import sample.Data.*;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable
{
    @FXML
    AnchorPane sort;

    @FXML
    ComboBox sortchoice;
    @FXML
    Button sortgo, sortseed;
    @FXML
    TextField sortinput;
    @FXML
    Label sortErrorLabel;
    @FXML
    TextField sort_algo;
    @FXML
    WebView algorithm;

    String address;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        InItSortingTab();
    }

    public void InItSortingTab()
    {
        sort.setOnMouseClicked(new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent event)
            {
                System.out.println("Hello clickers!!!");

            }
        });
        ObservableList<String> options =
                FXCollections.observableArrayList(
                        "Bubble Sort",
                        "Quick Sort",
                        "Selection Sort",
                        "Merge Sort",
                        "Insertion Sort",
                        "Counting Sort"
                );
        sortchoice.getItems().addAll(options);

        sortchoice.valueProperty().addListener(new ChangeListener<String>()
        {
            @Override
            public void changed(ObservableValue ov, String t, String t1)
            {
               address = t1;
                System.out.println(address + " is selected");
                sortseed.setVisible(true);

            }
        });

        sortseed.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event)
            {
                RandomSeedArray seed = new RandomSeedArray(10);
                String input = seed.getSeedArrayString();
                sortinput.setVisible(true);
                sortgo.setVisible(true);
                sortinput.setText(input);
                Array_Checker check = new Array_Checker(input);
               // Bubble_Sort bubble_sort = new Bubble_Sort(sort, check.getIntList());

                RectDesign rectD = new RectDesign(sort,check.getIntList());
               // Bubble_Sort bubble_sort = new Bubble_Sort(rectD);
            }
        });

        sortgo.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event)
            {
                Array_Checker check = new Array_Checker(sortinput.getText());
                if (check.getErrorDetected())
                {
                    System.out.println("***********");
                    sortErrorLabel.setVisible(true);
                    sortErrorLabel.setText(check.getErrorType());
                }
                else
                {
                    if (sortErrorLabel.isVisible())
                        sortErrorLabel.setVisible(false);

                    sortseed.setDisable(true);
                    sortinput.setDisable(true);
                    List<Integer> intList = check.getIntList();
                    RectDesign rectD = new RectDesign(sort,intList);
                    if(address.equals("Bubble Sort")) {
                        Bubble_Sort bubble_sort = new Bubble_Sort(rectD,algorithm);
                        bubble_sort.startBubbleSort();
                    }
                    else if(address.equals("Quick Sort")) {
                        Quick_Sort quick_sort = new Quick_Sort(rectD,algorithm);
                        quick_sort.startQuickSort();
                    }
                }

            }
        });
    }
}
