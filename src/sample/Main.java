package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application {

    private static Stage pStage;
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Parent root = FXMLLoader.load(getClass().getResource("DesignLayout/sample.fxml"));
        pStage= primaryStage;
        System.out.println("hello");
        primaryStage.setTitle("VisDa  Version:1.0");
        primaryStage.setScene(new Scene(root, 1000, 680));
        primaryStage.setResizable(false);
        primaryStage.setFullScreen(false);
        setPrimaryStage(primaryStage);
        primaryStage.show();

    }

    public static Stage getPrimaryStage() {
        return pStage;
    }

    private void setPrimaryStage(Stage pStage) {
       Main.pStage = pStage;
    }
    public static void main(String[] args) {
        launch(args);
    }
}
