package sample.Model;

import javafx.animation.AnimationTimer;
import javafx.animation.PathTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.shape.*;
import javafx.scene.text.Text;
import javafx.scene.transform.Translate;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Duration;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by AlZihad on 1/20/2017.
 */


public class RectangleShape {
    Group root;
    public Rectangle rectangle;
    public Text text;
    Timer timer, delayTimer;
    int time;
    RectangleShape rectShape;
    WebView algorithm;
    public RectangleShape(Group group, Rectangle rectangle, Text text, WebView algorithm)
    {
        this.algorithm = algorithm;
        this.rectangle = rectangle;
        this.root = group;
        this.text = text;
    }

    public void interChangePosition(RectangleShape rectShape, int time)
    {
        this.time = time;
        this.rectShape = rectShape;
        timer = new Timer();
        timer.schedule(new InterchangeTask(), time*1000);

    }

    public void addIntentionalDelay(int time)
    {
        delayTimer = new Timer();
        delayTimer.schedule(new DelayTask(), time*1000);
    }

    class InterchangeTask extends TimerTask
    {
        public void run()
        {
            System.out.println("Time's up!");
            System.out.println("Interchanged value between : " + rectShape.text.getText() + "  and  " + text.getText());
            coloredAlgo(4);
            double x = rectangle.getX();
            double y = rectShape.rectangle.getX();
            rectangle.setX(y);
            rectShape.rectangle.setX(x);
            double yy = text.getX();
            text.setX(rectShape.text.getX());
            rectShape.text.setX(yy);
            timer.cancel(); //Terminate the timer thread
        }
    }
    class DelayTask extends TimerTask
    {
        public void run()
        {
            System.out.println("Delay ended");
            coloredAlgo(6);
        }
    }

    public void coloredAlgo(int line)
    {
        Platform.runLater(() -> {
            // Update UI here.
            String color[]={"#FFFFB0","#FFFFB0","#FFFFB0","#FFFFB0","#FFFFB0"};
            for (int i=1;i<=5;i++)
            {
                if (line ==i)
                {
                    color[i-1]="#FFFF00";//System.out.println(color[i-1]);
                }
                else
                {
                    color[i-1]="#FFFFA0";//System.out.println(color[i-1]);
                }
            }
            System.out.println("UI UPDATED");
            WebEngine webEngine = algorithm.getEngine();
            webEngine.loadContent("<html>\n" +
                    "<head>\n" +
                    "\t<title></title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<p id=\"code1\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;\"><span style=\"font-size:14px;\"><span data-mce-style=\"background-color: #ffffff;\"><span style=\"background-color:"+color[0]+";\">do</span></span></span></p>\n" +
                    "\n" +
                    "<p id=\"code2\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;\"><span style=\"font-size:14px;\"><span data-mce-style=\"background-color: #ffffff;\"><span style=\"background-color:"+color[1]+";\">&nbsp;for i = 1 to LastUnsortedElement</span></p>\n" +
                    "\n" +
                    "<p id=\"code4\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;\"><span style=\"font-size:14px;\"><span data-mce-style=\"background-color: #ffffff;\"><span style=\"background-color:"+color[2]+";\">&nbsp;&nbsp;&nbsp;&nbsp;if leftElement &gt; rightElement</span></span></span></p>\n" +
                    "\n" +
                    "<p id=\"code5\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;\"><span style=\"font-size:14px;\"><span data-mce-style=\"background-color: #ffffff;\"><span style=\"background-color:"+color[3]+";\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;swap(leftElement, rightElement)</span></span></span></p>\n" +
                    "\n" +
                    "<p id=\"code7\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;\"><span style=\"font-size:14px;\"><span data-mce-style=\"background-color: #ffffff;\"><span style=\"background-color:"+color[4]+";\">while swapped</span></span></span></p>\n" +
                    "</body>\n" +
                    "</html>\n");
        });


    }
}

